package pl.golebiewski.dataholders;

import pl.golebiewski.enums.CalibrationParameters;
import pl.golebiewski.enums.SensorParameters;
import pl.golebiewski.enums.StereoCalibrationParameters;

public class PropertiesHolder {

    private int pointsHorizontal;
    private int pointsVertical;
    private boolean fastCheck;
    private boolean adaptiveThreshold;
    private boolean normalizeImage;
    private boolean filterQuads;

    private double pixelSize;
    private double sensorHeight;
    private double sensorWidth;
    private double rightPixelSize;
    private double rightSensorHeight;
    private double rightSensorWidth;

    private SensorParameters sensorParameters;
    private CalibrationParameters calibrationParameters;
    private StereoCalibrationParameters stereoCalibrationParameters;

    private boolean rationalModel;
    private boolean thinPrismModel;
    private boolean tiltedModel;

    public PropertiesHolder() {
        this.pointsHorizontal = 9;
        this.pointsVertical = 6;
        this.fastCheck = true;
        this.adaptiveThreshold = false;
        this.normalizeImage = false;
        this.filterQuads = false;
        this.sensorParameters = SensorParameters.UNKNOWN;
        this.calibrationParameters = CalibrationParameters.EXTENDED;
        this.stereoCalibrationParameters = StereoCalibrationParameters.SAME_CAMERA;
        this.rationalModel = false;
        this.thinPrismModel = false;
        this.tiltedModel = false;
    }

    public int getPointsHorizontal() {
        return pointsHorizontal;
    }

    public void setPointsHorizontal(int pointsHorizontal) {
        this.pointsHorizontal = pointsHorizontal;
    }

    public int getPointsVertical() {
        return pointsVertical;
    }

    public void setPointsVertical(int pointsVertical) {
        this.pointsVertical = pointsVertical;
    }

    public boolean isFastCheck() {
        return fastCheck;
    }

    public void setFastCheck(boolean fastCheck) {
        this.fastCheck = fastCheck;
    }

    public boolean isAdaptiveThreshold() {
        return adaptiveThreshold;
    }

    public void setAdaptiveThreshold(boolean adaptiveThreshold) {
        this.adaptiveThreshold = adaptiveThreshold;
    }

    public boolean isNormalizeImage() {
        return normalizeImage;
    }

    public void setNormalizeImage(boolean normalizeImage) {
        this.normalizeImage = normalizeImage;
    }

    public boolean isFilterQuads() {
        return filterQuads;
    }

    public void setFilterQuads(boolean filterQuads) {
        this.filterQuads = filterQuads;
    }

    public double getPixelSize() {
        return pixelSize;
    }

    public void setPixelSize(double pixelSize) {
        this.pixelSize = pixelSize;
    }

    public double getSensorHeight() {
        return sensorHeight;
    }

    public void setSensorHeight(double sensorHeight) {
        this.sensorHeight = sensorHeight;
    }

    public double getSensorWidth() {
        return sensorWidth;
    }

    public void setSensorWidth(double sensorWidth) {
        this.sensorWidth = sensorWidth;
    }

    public double getRightPixelSize() {
        return rightPixelSize;
    }

    public void setRightPixelSize(double rightPixelSize) {
        this.rightPixelSize = rightPixelSize;
    }

    public double getRightSensorHeight() {
        return rightSensorHeight;
    }

    public void setRightSensorHeight(double rightSensorHeight) {
        this.rightSensorHeight = rightSensorHeight;
    }

    public double getRightSensorWidth() {
        return rightSensorWidth;
    }

    public void setRightSensorWidth(double rightSensorWidth) {
        this.rightSensorWidth = rightSensorWidth;
    }

    public SensorParameters getSensorParameters() {
        return sensorParameters;
    }

    public void setSensorParameters(SensorParameters sensorParameters) {
        this.sensorParameters = sensorParameters;
    }

    public CalibrationParameters getCalibrationParameters() {
        return calibrationParameters;
    }

    public void setCalibrationParameters(CalibrationParameters calibrationParameters) {
        this.calibrationParameters = calibrationParameters;
    }

    public StereoCalibrationParameters getStereoCalibrationParameters() {
        return stereoCalibrationParameters;
    }

    public void setStereoCalibrationParameters(StereoCalibrationParameters stereoCalibrationParameters) {
        this.stereoCalibrationParameters = stereoCalibrationParameters;
    }

    public boolean isRationalModel() {
        return rationalModel;
    }

    public void setRationalModel(boolean rationalModel) {
        this.rationalModel = rationalModel;
    }

    public boolean isThinPrismModel() {
        return thinPrismModel;
    }

    public void setThinPrismModel(boolean thinPrismModel) {
        this.thinPrismModel = thinPrismModel;
    }

    public boolean isTiltedModel() {
        return tiltedModel;
    }

    public void setTiltedModel(boolean tiltedModel) {
        this.tiltedModel = tiltedModel;
    }

}
