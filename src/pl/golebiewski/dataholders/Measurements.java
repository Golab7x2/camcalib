package pl.golebiewski.dataholders;

public class Measurements {

    private int pointId;
    private double imageX;
    private double imageY;
    private double objectX;
    private double objectY;
    private double objectZ;

    public Measurements(int pointId, double imageX, double imageY, double objectX, double objectY, double objectZ) {
        this.pointId = pointId;
        this.imageX = imageX;
        this.imageY = imageY;
        this.objectX = objectX;
        this.objectY = objectY;
        this.objectZ = objectZ;
    }

    public int getPointId() {
        return pointId;
    }

    public void setPointId(int pointId) {
        this.pointId = pointId;
    }

    public double getImageX() {
        return imageX;
    }

    public void setImageX(double imageX) {
        this.imageX = imageX;
    }

    public double getImageY() {
        return imageY;
    }

    public void setImageY(double imageY) {
        this.imageY = imageY;
    }

    public double getObjectX() {
        return objectX;
    }

    public void setObjectX(double objectX) {
        this.objectX = objectX;
    }

    public double getObjectY() {
        return objectY;
    }

    public void setObjectY(double objectY) {
        this.objectY = objectY;
    }

    public double getObjectZ() {
        return objectZ;
    }

    public void setObjectZ(double objectZ) {
        this.objectZ = objectZ;
    }
}
