package pl.golebiewski.dataholders;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import java.util.ArrayList;
import java.util.List;

public class ResultsHolder {

    private Mat cameraMatrix;
    private Mat distortionCoefficients;
    private List<Mat> rvecs; //Vector of rotation vectors estimated for each pattern view
    private List<Mat> tvecs; //Vector of translation vectors estimated for each pattern view
    private Mat stdDevIntrinsics;
    private Mat stdDevExtrinsics;
    private Mat perViewErrors;
    private double rmsCalibrationError;

    //for stereo calibration
    private Mat rotationMatrix; //Rotation matrix between 1st and 2nd camera coordinate systems
    private Mat translationVector; //Translation vector between the coordinate systems of the cameras
    private Mat essentialMatrix;
    private Mat fundamentalMatrix;

    public ResultsHolder() {
        this.cameraMatrix = new Mat(3, 3, CvType.CV_32FC1);
        cameraMatrix.put(0, 0, 1);
        cameraMatrix.put(1, 1, 1);
        this.distortionCoefficients = new Mat();
        this.rvecs = new ArrayList<>();
        this.tvecs = new ArrayList<>();
        this.stdDevIntrinsics = new Mat();
        this.stdDevExtrinsics = new Mat();
        this.perViewErrors = new Mat();
        this.rotationMatrix = new Mat(3, 3, CvType.CV_32FC1);
        this.translationVector = new Mat();
        this.essentialMatrix = new Mat(3, 3, CvType.CV_32FC1);
        this.fundamentalMatrix = new Mat(3, 3, CvType.CV_32FC1);
    }

    public Mat getCameraMatrix() {
        return cameraMatrix;
    }

    public Mat getDistortionCoefficients() {
        return distortionCoefficients;
    }

    public List<Mat> getRvecs() {
        return rvecs;
    }

    public List<Mat> getTvecs() {
        return tvecs;
    }

    public Mat getStdDevIntrinsics() {
        return stdDevIntrinsics;
    }

    public Mat getStdDevExtrinsics() {
        return stdDevExtrinsics;
    }

    public Mat getPerViewErrors() {
        return perViewErrors;
    }

    public double getRmsCalibrationError() {
        return rmsCalibrationError;
    }

    public void setRmsCalibrationError(double rmsCalibrationError) {
        this.rmsCalibrationError = rmsCalibrationError;
    }

    public Mat getRotationMatrix() {
        return rotationMatrix;
    }

    public Mat getTranslationVector() {
        return translationVector;
    }

    public Mat getEssentialMatrix() {
        return essentialMatrix;
    }

    public Mat getFundamentalMatrix() {
        return fundamentalMatrix;
    }

}
