package pl.golebiewski.enums;

public enum CalibrationParameters {
    SIMPLE,
    EXTENDED
}
