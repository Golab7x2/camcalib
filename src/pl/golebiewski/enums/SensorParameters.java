package pl.golebiewski.enums;

public enum SensorParameters {
    PIXEL_SIZE,
    SENSOR_DIMENSIONS,
    UNKNOWN
}
