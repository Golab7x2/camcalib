package pl.golebiewski.enums;

public enum StereoCalibrationParameters {
    SAME_CAMERA,
    DIFFERENT_CAMERAS
}
