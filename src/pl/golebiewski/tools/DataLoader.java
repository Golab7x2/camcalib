package pl.golebiewski.tools;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import java.io.File;
import java.util.List;

public class DataLoader {

    public static List<File> loadImages(Window owner) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load images...");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp"),
                new FileChooser.ExtensionFilter("All files", "*"));
        return fileChooser.showOpenMultipleDialog(owner);
    }

    public static File loadCalibFile(Window window) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load calib file...");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Calib", "*.calib")
        );
        return fileChooser.showOpenDialog(window);
    }

    public static File loadObjectPoints(Window window) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load object points...");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Object points", "*.csv")
        );
        return fileChooser.showOpenDialog(window);
    }

}
