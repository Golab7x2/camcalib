package pl.golebiewski.tools;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pl.golebiewski.controllers.*;
import pl.golebiewski.dataholders.PropertiesHolder;


public class StageCreator {

    public void createMainModuleStage(String title, String stageFxmlPath, int width, int height) {
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(stageFxmlPath));
            Parent root = fxmlLoader.load();
            stage.getIcons().add(new Image("/resources/icon-camera.png"));
            stage.setScene(new Scene(root, width, height));
            stage.setTitle(title);
            stage.show();
        }
        catch (Exception e) {
            AlertCreator.errorAlert("Cannot find file: " + stageFxmlPath, stage.getOwner());
        }
    }

    public void createImageViewer(Image img){
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/pl/golebiewski/stages/image-viewer-stage.fxml"));
            Parent root = fxmlLoader.load();
            ImageViewerController imageViewerController = fxmlLoader.getController();
            imageViewerController.setImage(img);
            stage.getIcons().add(new Image("/resources/icon-camera.png"));
            stage.setScene(new Scene(root, 800, 600));
            stage.setTitle("Image Viewer");
            stage.show();
        }
        catch (Exception e) {
            AlertCreator.errorAlert("Cannot find file: image-viewer-stage.fxml", stage.getOwner());
        }
    }

    public void createAutomaticProperties(PropertiesHolder propertiesHolder) {
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/pl/golebiewski/stages/automatic-properties-stage.fxml"));
            Parent root = fxmlLoader.load();
            AutomaticPropertiesController automaticPropertiesController = fxmlLoader.getController();
            automaticPropertiesController.setPropertiesHolder(propertiesHolder);
            stage.getIcons().add(new Image("/resources/icon-settings.png"));
            stage.setScene(new Scene(root, 480, 475));
            stage.setResizable(false);
            stage.setTitle("Project properties");
            stage.show();
        }
        catch (Exception e) {
            AlertCreator.errorAlert("Cannot find file: automatic-properties-stage.fxml", stage.getOwner());
        }
    }

    public void createStereoProperties(PropertiesHolder propertiesHolder) {
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/pl/golebiewski/stages/stereo-properties-stage.fxml"));
            Parent root = fxmlLoader.load();
            StereoPropertiesController stereoPropertiesController = fxmlLoader.getController();
            stereoPropertiesController.setPropertiesHolder(propertiesHolder);
            stage.getIcons().add(new Image("/resources/icon-settings.png"));
            stage.setScene(new Scene(root, 480, 590));
            stage.setResizable(false);
            stage.setTitle("Project properties");
            stage.show();
        }
        catch (Exception e) {
            AlertCreator.errorAlert("Cannot find file: stereo-properties-stage.fxml", stage.getOwner());
        }
    }

    public void createAboutStage() {
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/pl/golebiewski/stages/about-stage.fxml"));
            Parent root = fxmlLoader.load();
            stage.getIcons().add(new Image("/resources/icon-settings.png"));
            stage.setScene(new Scene(root, 600, 200));
            stage.setTitle("About");
            stage.show();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            AlertCreator.errorAlert("Cannot find file: about-stage.fxml", stage.getOwner());
        }
    }

}
