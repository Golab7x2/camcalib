package pl.golebiewski.tools;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import pl.golebiewski.dataholders.PropertiesHolder;
import pl.golebiewski.dataholders.ResultsHolder;
import pl.golebiewski.enums.CalibrationParameters;
import pl.golebiewski.enums.SensorParameters;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ResultsPrinter {

    //taken from controller class
    private List<File> fileList;
    private List<Boolean> successList;
    private PropertiesHolder propertiesHolder;
    private ResultsHolder resultsHolder;
    private Size imageSize;

    //only for stereo calibration
    private List<File> leftFileList;
    private List<File> rightFileList;
    private ResultsHolder leftResultsHolder;
    private ResultsHolder rightResultsHolder;
    private Size leftImageSize;
    private Size rightImageSize;


    public ResultsPrinter(List<File> fileList, List<Boolean> successList, PropertiesHolder propertiesHolder, ResultsHolder resultsHolder, Size imageSize) {
        this.fileList = fileList;
        this.successList = successList;
        this.propertiesHolder = propertiesHolder;
        this.resultsHolder = resultsHolder;
        this.imageSize = imageSize;
    }

    public ResultsPrinter(List<File> leftFileList, List<File> rightFileList, List<Boolean> successList, PropertiesHolder propertiesHolder, ResultsHolder leftResultsHolder, ResultsHolder rightResultsHolder, Size leftImageSize, Size rightImageSize) {
        this.leftFileList = leftFileList;
        this.rightFileList = rightFileList;
        this.successList = successList;
        this.propertiesHolder = propertiesHolder;
        this.leftResultsHolder = leftResultsHolder;
        this.rightResultsHolder = rightResultsHolder;
        this.leftImageSize = leftImageSize;
        this.rightImageSize = rightImageSize;
    }

    public Text[] printResults() {
        List<Text> finalResultsList = new ArrayList<>();

        Text mainError = new Text("MEAN CALIBRATION ERROR:\t" + round(resultsHolder.getRmsCalibrationError()).toPlainString());
        mainError.setFont(Font.font("default", FontWeight.BOLD, 14));
        finalResultsList.add(mainError);

        Text header1 = new Text("\r\n\r\nCAMERA");
        header1.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results1;
        results1 = "\r\nfx:\t" + round(resultsHolder.getCameraMatrix().get(0, 0)[0]).toPlainString()
                + "\r\nfy:\t" + round(resultsHolder.getCameraMatrix().get(1, 1)[0]).toPlainString()
                + "\r\ncx:\t" + round(resultsHolder.getCameraMatrix().get(0, 2)[0]).toPlainString()
                + "\r\ncy:\t" + round(resultsHolder.getCameraMatrix().get(1, 2)[0]).toPlainString();
        finalResultsList.add(header1);
        finalResultsList.add(new Text(results1));

        if(propertiesHolder.getSensorParameters() != SensorParameters.UNKNOWN) {
            Text units = new Text("\r\n\r\nCAMERA - world units");
            units.setFont(Font.font("default", FontWeight.BOLD, 14));
            Text t = calculateFocalLength(imageSize, resultsHolder);
            finalResultsList.add(units);
            finalResultsList.add(t);
        }

        Text header2 = new Text("\r\n\r\nDISTORTION COEFFICIENTS");
        header2.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results2;
        results2 = "\r\nk1:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 0)[0]).toPlainString()
                + "\r\nk2:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 1)[0]).toPlainString()
                + "\r\nk3:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 4)[0]).toPlainString();

        if(propertiesHolder.isRationalModel())
            results2 += "\r\nk4:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 5)[0]).toPlainString()
                + "\r\nk5:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 6)[0]).toPlainString()
                + "\r\nk6:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 7)[0]).toPlainString();

        results2 += "\r\np1:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 2)[0]).toPlainString()
                + "\r\np2:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 3)[0]).toPlainString();

        if(propertiesHolder.isThinPrismModel())
            results2 += "\r\ns1:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 8)[0]).toPlainString()
                + "\r\ns2:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 9)[0]).toPlainString()
                + "\r\ns3:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 10)[0]).toPlainString()
                + "\r\ns4:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 11)[0]).toPlainString();

        if(propertiesHolder.isTiltedModel())
            results2 += "\r\nτX:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 12)[0]).toPlainString()
                + "\r\nτY:\t" + round(resultsHolder.getDistortionCoefficients().get(0, 13)[0]).toPlainString();

        finalResultsList.add(header2);
        finalResultsList.add(new Text(results2));

        if(resultsHolder.getRvecs().size() != successList.size()) {
            for(int i = 0; i < successList.size(); i++) {
                //insert empty Mat if pattern is NOT recognized for specific image
                if(!successList.get(i)) {
                    resultsHolder.getRvecs().add(i, new Mat());
                    resultsHolder.getTvecs().add(i, new Mat());
                }
            }
        }

        Text header3 = new Text("\r\n\r\nROTATION VECTORS");
        header3.setFont(Font.font("default", FontWeight.BOLD, 14));
        Text header4 = new Text("\r\n\r\nTRANSLATION VECTORS");
        header4.setFont(Font.font("default", FontWeight.BOLD, 14));
        StringBuilder rvecsStringBuilder = new StringBuilder();
        StringBuilder tvecsStringBuilder = new StringBuilder();
        for(int i = 0; i < successList.size(); i++) {
            //prints rvecs
            if(successList.get(i)) rvecsStringBuilder
                    .append("\r\n")
                    .append(fileList.get(i).getName())
                    .append("\t")
                    .append(round(resultsHolder.getRvecs().get(i).get(0, 0)[0]).toPlainString())
                    .append("\t")
                    .append(round(resultsHolder.getRvecs().get(i).get(1, 0)[0]).toPlainString())
                    .append("\t")
                    .append(round(resultsHolder.getRvecs().get(i).get(2, 0)[0]).toPlainString());
            else rvecsStringBuilder
                    .append("\r\n")
                    .append(fileList.get(i).getName())
                    .append("\tUNDETERMINED");
            //prints tvecs
            if(successList.get(i)) tvecsStringBuilder
                    .append("\r\n")
                    .append(fileList.get(i).getName())
                    .append("\t")
                    .append(round(resultsHolder.getTvecs().get(i).get(0, 0)[0]).toPlainString())
                    .append("\t")
                    .append(round(resultsHolder.getTvecs().get(i).get(1, 0)[0]).toPlainString())
                    .append("\t")
                    .append(round(resultsHolder.getTvecs().get(i).get(2, 0)[0]).toPlainString());
            else tvecsStringBuilder
                    .append("\r\n")
                    .append(fileList.get(i).getName())
                    .append("\tUNDETERMINED");
        }
        String results3 = rvecsStringBuilder.toString();
        String results4 = tvecsStringBuilder.toString();
        finalResultsList.add(header3);
        finalResultsList.add(new Text(results3));
        finalResultsList.add(header4);
        finalResultsList.add(new Text(results4));

        if(propertiesHolder.getCalibrationParameters() == CalibrationParameters.EXTENDED) {
            Text header5 = new Text("\r\n\r\nSTANDARD DEVIATIONS INTRINSICS");
            header5.setFont(Font.font("default", FontWeight.BOLD, 14));
            String results5 = "\r\nfx:\t" + round(resultsHolder.getStdDevIntrinsics().get(0, 0)[0]).toPlainString()
                    + "\r\nfy:\t" + round(resultsHolder.getStdDevIntrinsics().get(1, 0)[0]).toPlainString()
                    + "\r\ncx:\t" + round(resultsHolder.getStdDevIntrinsics().get(2, 0)[0]).toPlainString()
                    + "\r\ncy:\t" + round(resultsHolder.getStdDevIntrinsics().get(3, 0)[0]).toPlainString()
                    + "\r\nk1:\t" + round(resultsHolder.getStdDevIntrinsics().get(4, 0)[0]).toPlainString()
                    + "\r\nk2:\t" + round(resultsHolder.getStdDevIntrinsics().get(5, 0)[0]).toPlainString()
                    + "\r\nk3:\t" + round(resultsHolder.getStdDevIntrinsics().get(8, 0)[0]).toPlainString();

            if(propertiesHolder.isRationalModel())
                results5 += "\r\nk4:\t" + round(resultsHolder.getStdDevIntrinsics().get(9, 0)[0]).toPlainString()
                    + "\r\nk5:\t" + round(resultsHolder.getStdDevIntrinsics().get(10, 0)[0]).toPlainString()
                    + "\r\nk6:\t" + round(resultsHolder.getStdDevIntrinsics().get(11, 0)[0]).toPlainString();

            results5 += "\r\np1:\t" + round(resultsHolder.getStdDevIntrinsics().get(6, 0)[0]).toPlainString()
                    + "\r\np2:\t" + round(resultsHolder.getStdDevIntrinsics().get(7, 0)[0]).toPlainString();

            if(propertiesHolder.isThinPrismModel())
                results5 += "\r\ns1:\t" + round(resultsHolder.getStdDevIntrinsics().get(12, 0)[0]).toPlainString()
                        + "\r\ns2:\t" + round(resultsHolder.getStdDevIntrinsics().get(13, 0)[0]).toPlainString()
                        + "\r\ns3:\t" + round(resultsHolder.getStdDevIntrinsics().get(14, 0)[0]).toPlainString()
                        + "\r\ns4:\t" + round(resultsHolder.getStdDevIntrinsics().get(15, 0)[0]).toPlainString();

            if(propertiesHolder.isTiltedModel())
                results5 += "\r\nτX:\t" + round(resultsHolder.getStdDevIntrinsics().get(16, 0)[0]).toPlainString()
                        + "\r\nτY:\t" + round(resultsHolder.getStdDevIntrinsics().get(17, 0)[0]).toPlainString();

            finalResultsList.add(header5);
            finalResultsList.add(new Text(results5));

            //divide StdDevExtrinsics Mat into separate vectors for each image
            int x = (int) resultsHolder.getStdDevExtrinsics().size().height;
            x = x/6;
            List<Mat> stdDevExtrinsicsList = new ArrayList<>();
            List<Double> perViewErrorsList = new ArrayList<>();
            for(int i = 0; i < x; i++) {
                Mat m = new Mat(6, 1, CvType.CV_64FC1);
                m.put(0, 0, resultsHolder.getStdDevExtrinsics().get(6 * i, 0)[0]);
                m.put(1, 0, resultsHolder.getStdDevExtrinsics().get(6 * i + 1, 0)[0]);
                m.put(2, 0, resultsHolder.getStdDevExtrinsics().get(6 * i + 2, 0)[0]);
                m.put(3, 0, resultsHolder.getStdDevExtrinsics().get(6 * i + 3, 0)[0]);
                m.put(4, 0,resultsHolder.getStdDevExtrinsics().get(6 * i + 4, 0)[0]);
                m.put(5, 0, resultsHolder.getStdDevExtrinsics().get(6 * i + 5, 0)[0]);
                stdDevExtrinsicsList.add(m);

                perViewErrorsList.add(resultsHolder.getPerViewErrors().get(i, 0)[0]);
            }

            //list size HAVE TO be equal to the number of files
            if(stdDevExtrinsicsList.size() != fileList.size()) {
                for(int i = 0; i < fileList.size(); i++) {
                    if(!successList.get(i)) {
                        stdDevExtrinsicsList.add(i, new Mat(6, 1, CvType.CV_64FC1));
                        perViewErrorsList.add(i, 0.0);
                    }
                }
            }

            Text header6 = new Text("\r\n\r\nSTANDARD DEVIATIONS EXTRINSICS");
            header6.setFont(Font.font("default", FontWeight.BOLD, 14));
            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < fileList.size(); i++) {
                if(successList.get(i)) stringBuilder
                        .append("\r\n")
                        .append(fileList.get(i).getName())
                        .append("\tROTATION\t")
                        .append(round(stdDevExtrinsicsList.get(i).get(0, 0)[0]).toPlainString())
                        .append("\t")
                        .append(round(stdDevExtrinsicsList.get(i).get(1, 0)[0]).toPlainString())
                        .append("\t")
                        .append(round(stdDevExtrinsicsList.get(i).get(2, 0)[0]).toPlainString())
                        .append("\tTRANSLATION\t")
                        .append(round(stdDevExtrinsicsList.get(i).get(3, 0)[0]).toPlainString())
                        .append("\t")
                        .append(round(stdDevExtrinsicsList.get(i).get(4, 0)[0]).toPlainString())
                        .append("\t")
                        .append(round(stdDevExtrinsicsList.get(i).get(5, 0)[0]).toPlainString());
                else stringBuilder
                        .append("\r\n")
                        .append(fileList.get(i).getName())
                        .append("\tUNDETERMINED");
            }
            String results6 = stringBuilder.toString();
            finalResultsList.add(header6);
            finalResultsList.add(new Text(results6));

            Text header7 = new Text("\r\n\r\nRMS PER VIEW");
            header7.setFont(Font.font("default", FontWeight.BOLD, 14));
            stringBuilder = new StringBuilder();
            for(int i = 0; i < fileList.size(); i++) {
                if(successList.get(i)) stringBuilder
                        .append("\r\n")
                        .append(fileList.get(i).getName())
                        .append("\t")
                        .append(round(perViewErrorsList.get(i)).toPlainString());
                else stringBuilder
                        .append("\r\n")
                        .append(fileList.get(i).getName())
                        .append("\tUNDETERMINED");
            }
            String results7 = stringBuilder.toString();
            finalResultsList.add(header7);
            finalResultsList.add(new Text(results7));
        }

        Text header8 = new Text("\r\n\r\nCALIBRATION TARGET");
        header8.setFont(Font.font("default", FontWeight.BOLD, 14));
        StringBuilder succesStringBuilder = new StringBuilder();
        for(int i = 0; i < successList.size(); i++) {
            if(successList.get(i)) succesStringBuilder
                    .append("\r\n")
                    .append(fileList.get(i).getName())
                    .append("\tYES");
            else succesStringBuilder
                    .append("\r\n")
                    .append(fileList.get(i).getName())
                    .append("\tNO");
        }
        String results8 = succesStringBuilder.toString();
        finalResultsList.add(header8);
        finalResultsList.add(new Text(results8));

        return finalResultsList.toArray(new Text[0]);
    }

    public Text[] printStereoResults() {
        List<Text> finalResultsList = new ArrayList<>();

        Text mainError = new Text("MEAN CALIBRATION ERROR:\t" + round(leftResultsHolder.getRmsCalibrationError()).toPlainString());
        mainError.setFont(Font.font("default", FontWeight.BOLD, 14));
        finalResultsList.add(mainError);

        //left camera focal length and principal point coordinates
        Text header1 = new Text("\r\n\r\nLEFT CAMERA");
        header1.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results1;
        results1 = "\r\nfx:\t" + round(leftResultsHolder.getCameraMatrix().get(0, 0)[0]).toPlainString()
                + "\r\nfy:\t" + round(leftResultsHolder.getCameraMatrix().get(1, 1)[0]).toPlainString()
                + "\r\ncx:\t" + round(leftResultsHolder.getCameraMatrix().get(0, 2)[0]).toPlainString()
                + "\r\ncy:\t" + round(leftResultsHolder.getCameraMatrix().get(1, 2)[0]).toPlainString();
        finalResultsList.add(header1);
        finalResultsList.add(new Text(results1));

        if(propertiesHolder.getSensorParameters() != SensorParameters.UNKNOWN) {
            Text units = new Text("\r\n\r\nLEFT CAMERA - world units");
            units.setFont(Font.font("default", FontWeight.BOLD, 14));
            Text t = calculateFocalLength(leftImageSize, leftResultsHolder);
            finalResultsList.add(units);
            finalResultsList.add(t);
        }


        //left camera distortion coefficients
        Text header2 = new Text("\r\n\r\nLEFT CAMERA DISTORTION COEFFICIENTS");
        header2.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results2;
        results2 = "\r\nk1:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 0)[0]).toPlainString()
                + "\r\nk2:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 1)[0]).toPlainString()
                + "\r\nk3:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 4)[0]).toPlainString();

        if(propertiesHolder.isRationalModel())
            results2 += "\r\nk4:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 5)[0]).toPlainString()
                    + "\r\nk5:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 6)[0]).toPlainString()
                    + "\r\nk6:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 7)[0]).toPlainString();

        results2 += "\r\np1:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 2)[0]).toPlainString()
                + "\r\np2:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 3)[0]).toPlainString();

        if(propertiesHolder.isThinPrismModel())
            results2 += "\r\ns1:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 8)[0]).toPlainString()
                    + "\r\ns2:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 9)[0]).toPlainString()
                    + "\r\ns3:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 10)[0]).toPlainString()
                    + "\r\ns4:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 11)[0]).toPlainString();

        if(propertiesHolder.isTiltedModel())
            results2 += "\r\nτX:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 12)[0]).toPlainString()
                    + "\r\nτY:\t" + round(leftResultsHolder.getDistortionCoefficients().get(0, 13)[0]).toPlainString();

        finalResultsList.add(header2);
        finalResultsList.add(new Text(results2));

        //right camera focal length and principal point coordinates
        Text header3 = new Text("\r\n\r\nRIGHT CAMERA");
        header3.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results3;
        results3 = "\r\nfx:\t" + round(rightResultsHolder.getCameraMatrix().get(0, 0)[0]).toPlainString()
                + "\r\nfy:\t" + round(rightResultsHolder.getCameraMatrix().get(1, 1)[0]).toPlainString()
                + "\r\ncx:\t" + round(rightResultsHolder.getCameraMatrix().get(0, 2)[0]).toPlainString()
                + "\r\ncy:\t" + round(rightResultsHolder.getCameraMatrix().get(1, 2)[0]).toPlainString();
        finalResultsList.add(header3);
        finalResultsList.add(new Text(results3));

        if(propertiesHolder.getSensorParameters() != SensorParameters.UNKNOWN) {
            Text units = new Text("\r\n\r\nRIGHT CAMERA - world units");
            units.setFont(Font.font("default", FontWeight.BOLD, 14));
            Text t = calculateFocalLength(rightImageSize, rightResultsHolder);
            finalResultsList.add(units);
            finalResultsList.add(t);
        }

        //right camera distortion coefficients
        Text header4 = new Text("\r\n\r\nRIGHT CAMERA DISTORTION COEFFICIENTS");
        header4.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results4;
        results4 = "\r\nk1:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 0)[0]).toPlainString()
                + "\r\nk2:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 1)[0]).toPlainString()
                + "\r\nk3:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 4)[0]).toPlainString();

        if(propertiesHolder.isRationalModel())
            results4 += "\r\nk4:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 5)[0]).toPlainString()
                    + "\r\nk5:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 6)[0]).toPlainString()
                    + "\r\nk6:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 7)[0]).toPlainString();

        results4 += "\r\np1:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 2)[0]).toPlainString()
                + "\r\np2:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 3)[0]).toPlainString();

        if(propertiesHolder.isThinPrismModel())
            results4 += "\r\ns1:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 8)[0]).toPlainString()
                    + "\r\ns2:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 9)[0]).toPlainString()
                    + "\r\ns3:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 10)[0]).toPlainString()
                    + "\r\ns4:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 11)[0]).toPlainString();

        if(propertiesHolder.isTiltedModel())
            results4 += "\r\nτX:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 12)[0]).toPlainString()
                    + "\r\nτY:\t" + round(rightResultsHolder.getDistortionCoefficients().get(0, 13)[0]).toPlainString();

        finalResultsList.add(header4);
        finalResultsList.add(new Text(results4));

        //rotation matrix
        //essential matrix
        //fundamental matrix
        Text header5 = new Text("\r\n\r\nROTATION MATRIX");
        header5.setFont(Font.font("default", FontWeight.BOLD, 14));
        StringBuilder rotationStringBuilder = new StringBuilder();
        StringBuilder essentialStringBuilder = new StringBuilder();
        StringBuilder fundamentalStringBuilder = new StringBuilder();
        for(int i = 0; i < 3; i++) {
            rotationStringBuilder
                    .append("\r\n")
                    .append(round(leftResultsHolder.getRotationMatrix().get(i, 0)[0]).toPlainString())
                    .append("\t\t|\t")
                    .append(round(leftResultsHolder.getRotationMatrix().get(i, 1)[0]).toPlainString())
                    .append("\t\t|\t")
                    .append(round(leftResultsHolder.getRotationMatrix().get(i, 2)[0]).toPlainString());

            essentialStringBuilder
                    .append("\r\n")
                    .append(round(leftResultsHolder.getEssentialMatrix().get(i, 0)[0]).toPlainString())
                    .append("\t\t|\t")
                    .append(round(leftResultsHolder.getEssentialMatrix().get(i, 1)[0]).toPlainString())
                    .append("\t\t|\t")
                    .append(round(leftResultsHolder.getEssentialMatrix().get(i, 2)[0]).toPlainString());

            fundamentalStringBuilder
                    .append("\r\n")
                    .append(round(leftResultsHolder.getFundamentalMatrix().get(i, 0)[0]).toPlainString())
                    .append("\t\t|\t")
                    .append(round(leftResultsHolder.getFundamentalMatrix().get(i, 1)[0]).toPlainString())
                    .append("\t\t|\t")
                    .append(round(leftResultsHolder.getFundamentalMatrix().get(i, 2)[0]).toPlainString());
        }

        finalResultsList.add(header5);
        String results5 = rotationStringBuilder.toString();
        finalResultsList.add(new Text(results5));

        Text header6 = new Text("\r\n\r\nTRANSLATION VECTOR");
        header6.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results6 = "\r\n" + round(leftResultsHolder.getTranslationVector().get(0, 0)[0]).toPlainString()
                + "\r\n" + round(leftResultsHolder.getTranslationVector().get(1, 0)[0]).toPlainString()
                + "\r\n" + round(leftResultsHolder.getTranslationVector().get(2, 0)[0]).toPlainString();

        Text header7 = new Text("\r\n\r\nESSENTIAL MATRIX");
        header7.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results7 = essentialStringBuilder.toString();

        Text header8 = new Text("\r\n\r\nFUNDAMENTAL MATRIX");
        header8.setFont(Font.font("default", FontWeight.BOLD, 14));
        String results8 = fundamentalStringBuilder.toString();

        finalResultsList.add(header6);
        finalResultsList.add(new Text(results6));
        finalResultsList.add(header7);
        finalResultsList.add(new Text(results7));
        finalResultsList.add(header8);
        finalResultsList.add(new Text(results8));

        if(propertiesHolder.getCalibrationParameters() == CalibrationParameters.EXTENDED) {
            Text header9 = new Text("\r\n\r\nRMS PER VIEW");
            header9.setFont(Font.font("default", FontWeight.BOLD, 14));

            int x = (int) leftResultsHolder.getPerViewErrors().size().height;
            List<Double> perViewErrorsList = new ArrayList<>();
            for(int i = 0; i < x; i++) {
                perViewErrorsList.add(leftResultsHolder.getPerViewErrors().get(i, 0)[0]);
            }

            //list size HAVE TO be equal to the number of files
            if(perViewErrorsList.size() != leftFileList.size()) {
                for(int i = 0; i < leftFileList.size(); i++) {
                    if(!successList.get(i)) {
                        perViewErrorsList.add(i, 0.0);
                    }
                }
            }

            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < leftFileList.size(); i++) {
                if(successList.get(i)) stringBuilder
                        .append("\r\n")
                        .append(leftFileList.get(i).getName())
                        .append("-")
                        .append(rightFileList.get(i).getName())
                        .append("\t\t")
                        .append(round(perViewErrorsList.get(i)).toPlainString());
                else stringBuilder
                        .append("\r\n")
                        .append(leftFileList.get(i).getName())
                        .append("-")
                        .append(rightFileList.get(i).getName())
                        .append("\t\tUNDETERMINED");
            }

            finalResultsList.add(header9);
            String results9 = stringBuilder.toString();
            finalResultsList.add(new Text(results9));
        }

        Text header10 = new Text("\r\n\r\nCALIBRATION TARGET");
        header10.setFont(Font.font("default", FontWeight.BOLD, 14));
        StringBuilder succesStringBuilder = new StringBuilder();
        for(int i = 0; i < successList.size(); i++) {
            if(successList.get(i)) succesStringBuilder
                    .append("\r\n")
                    .append(leftFileList.get(i).getName())
                    .append("-")
                    .append(rightFileList.get(i).getName())
                    .append("\t\tYES");
            else succesStringBuilder
                    .append("\r\n")
                    .append(leftFileList.get(i).getName())
                    .append("-")
                    .append(rightFileList.get(i).getName())
                    .append("\t\tNO");
        }
        String results10 = succesStringBuilder.toString();
        finalResultsList.add(header10);
        finalResultsList.add(new Text(results10));

        return finalResultsList.toArray(new Text[0]);
    }

    private Text calculateFocalLength(Size imageSize, ResultsHolder resultsHolder) {
        double width = 0.0;
        double height = 0.0;
        if(propertiesHolder.getSensorParameters() == SensorParameters.PIXEL_SIZE) {
            width = imageSize.width * propertiesHolder.getPixelSize() / 1000;
            height = imageSize.height * propertiesHolder.getPixelSize() / 1000;
        }
        else if(propertiesHolder.getSensorParameters() == SensorParameters.SENSOR_DIMENSIONS) {
            width = propertiesHolder.getSensorWidth();
            height = propertiesHolder.getSensorHeight();
        }

        double[] fovx = new double[1];
        double[] fovy = new double[1];
        double[] focalLength = new double[1];
        Point principalPoint = new Point();
        double[] aspectRatio = new double[1];

        Calib3d.calibrationMatrixValues(resultsHolder.getCameraMatrix(),
                imageSize,
                width,
                height,
                fovx,
                fovy,
                focalLength,
                principalPoint,
                aspectRatio);

        String s = "\r\nf:\t" + round(focalLength[0]).toPlainString()
                + "\r\ncx:\t" + round(principalPoint.x).toPlainString()
                + "\r\ncy:\t" + round(principalPoint.y).toPlainString()
                + "\r\nFOVx:\t" + round(fovx[0]).toPlainString()
                + "\r\nFOVy:\t" + round(fovy[0]).toPlainString();

        return new Text(s);
    }

    public void printResultsToFile(Window window) {
        Text[] resultsTextArray = this.printResults();
        StringBuilder stringBuilder = new StringBuilder();

        for(Text t : resultsTextArray) {
            stringBuilder.append(t.getText());
        }

        save(stringBuilder.toString(), window);

    }

    public void printStereoResultsToFile(Window window) {
        Text[] resultsTextArray = this.printStereoResults();
        StringBuilder stringBuilder = new StringBuilder();

        for(Text t : resultsTextArray) {
            stringBuilder.append(t.getText());
        }

        save(stringBuilder.toString(), window);
    }

    private void save(String preparedResults, Window window) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save calibration report");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Text files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(window);
        if(file != null) {
            try {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(preparedResults);
                fileWriter.close();
            } catch(IOException e) {
                AlertCreator.errorAlert("Error occurred while saving report", window);
            }
        }
    }

    public void saveCalibFile(Window window) {
        String calib = resultsHolder.getCameraMatrix().get(0, 0)[0]
                + "," + resultsHolder.getCameraMatrix().get(1, 1)[0]
                + "," + resultsHolder.getCameraMatrix().get(0, 2)[0]
                + "," + resultsHolder.getCameraMatrix().get(1, 2)[0];

        calib += "," + resultsHolder.getDistortionCoefficients().get(0, 0)[0]
                + "," + resultsHolder.getDistortionCoefficients().get(0, 1)[0]
                + "," + resultsHolder.getDistortionCoefficients().get(0, 2)[0]
                + "," + resultsHolder.getDistortionCoefficients().get(0, 3)[0]
                + "," + resultsHolder.getDistortionCoefficients().get(0, 4)[0];

        if(propertiesHolder.isRationalModel())
            calib += "," + resultsHolder.getDistortionCoefficients().get(0, 5)[0]
                    + "," + resultsHolder.getDistortionCoefficients().get(0, 6)[0]
                    + "," + resultsHolder.getDistortionCoefficients().get(0, 7)[0];

        if(propertiesHolder.isThinPrismModel())
            calib += "," + resultsHolder.getDistortionCoefficients().get(0, 8)[0]
                    + "," + resultsHolder.getDistortionCoefficients().get(0, 9)[0]
                    + "," + resultsHolder.getDistortionCoefficients().get(0, 10)[0]
                    + "," + resultsHolder.getDistortionCoefficients().get(0, 11)[0];

        if(propertiesHolder.isTiltedModel())
            calib += "," + resultsHolder.getDistortionCoefficients().get(0, 12)[0]
                    + "," + resultsHolder.getDistortionCoefficients().get(0, 13)[0];

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save calib file");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Calib file (*.calib)", "*.calib");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(window);
        if(file != null) {
            try {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(calib);
                fileWriter.close();
            } catch(IOException e) {
                AlertCreator.errorAlert("Error occurred while saving calib file", window);
            }
        }
    }

    private BigDecimal round(double d) {
        BigDecimal bd = new BigDecimal(d);
        return bd.setScale(14, BigDecimal.ROUND_HALF_UP);
    }

}
