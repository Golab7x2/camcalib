package pl.golebiewski.tools;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class ImageSaver {

    public static void saveUndistortedImages(Window owner, List<File> fileList, Mat cameraMatrix, Mat distortionCoefficients) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initOwner(owner);
        alert.setTitle("Save undistorted");
        alert.setHeaderText(null);
        alert.setContentText("Choose file format");
        ButtonType jpgBT = new ButtonType("JPG");
        ButtonType pngBT = new ButtonType("PNG");
        ButtonType gifBT = new ButtonType("GIF");
        ButtonType cancelBT = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(jpgBT, pngBT, gifBT, cancelBT);

        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Save undistorted");

        Optional<ButtonType> result = alert.showAndWait();
        String format = "";
        File selectedDirectory;
        //noinspection ConstantConditions
        if(result.get() == jpgBT) {
            format = "jpg";
            selectedDirectory = chooser.showDialog(owner);
        }
        else if(result.get() == pngBT) {
            format = "png";
            selectedDirectory = chooser.showDialog(owner);
        }
        else if(result.get() == gifBT) {
            format = "gif";
            selectedDirectory = chooser.showDialog(owner);
        } else selectedDirectory = null;

        if(selectedDirectory != null) {
            for(File f : fileList) {
                //undistort image
                Mat distortedMat = Imgcodecs.imread(f.getAbsolutePath());
                Mat undistortedMat = new Mat();
                Imgproc.undistort(
                        distortedMat,
                        undistortedMat,
                        cameraMatrix,
                        distortionCoefficients);

                //save temporary file
                Imgcodecs.imwrite("temp\\undistorted-temp-" + f.getName(), undistortedMat);
                File imageToConvert = new File("temp\\undistorted-temp-" + f.getName());

                //save image
                BufferedImage toSaveImage;
                try {
                toSaveImage = ImageIO.read(imageToConvert);
                String path = selectedDirectory.getAbsolutePath() + "\\undistorted-" + f.getName();
                path = path.substring(0, path.length()-3);
                path += format;
                File pathFile = new File(path);
                ImageIO.write(toSaveImage, format, pathFile);
                } catch(IOException e) {
                    AlertCreator.errorAlert("Error occurred while saving files", owner);
                }

                //release memory and delete temporary files
                //noinspection ResultOfMethodCallIgnored
                imageToConvert.delete();
                distortedMat.release();
                undistortedMat.release();
            }
        }
    }

}
