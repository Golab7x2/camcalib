package pl.golebiewski.tools;

import javafx.scene.control.Alert;
import javafx.stage.Window;

public class AlertCreator {

    public static void errorAlert(String message, Window owner) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    public static void informationAlert(String message, Window owner) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.showAndWait();
    }

}
