package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pl.golebiewski.dataholders.PropertiesHolder;
import java.net.URL;
import java.util.ResourceBundle;

public class AutomaticPropertiesController extends PropertiesController implements Initializable {

    @FXML TextField pointsHorizontalTextField;
    @FXML TextField pointsVerticalTextField;
    @FXML CheckBox fastCheckBox;
    @FXML CheckBox adaptiveCheckBox;
    @FXML CheckBox normalizeCheckBox;
    @FXML CheckBox quadsCheckBox;

    private PropertiesHolder propertiesHolder;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
    }

    public void saveProperties() {
        super.saveProperties();
        try {
            //Calibration pattern
            int pointsHorizontal = Integer.parseInt(pointsHorizontalTextField.getText());
            int pointVertical = Integer.parseInt(pointsVerticalTextField.getText());
            propertiesHolder.setPointsHorizontal(pointsHorizontal);
            propertiesHolder.setPointsVertical(pointVertical);

            if(fastCheckBox.isSelected()) propertiesHolder.setFastCheck(true);
            else propertiesHolder.setFastCheck(false);
            if(adaptiveCheckBox.isSelected()) propertiesHolder.setAdaptiveThreshold(true);
            else propertiesHolder.setAdaptiveThreshold(false);
            if(normalizeCheckBox.isSelected()) propertiesHolder.setNormalizeImage(true);
            else propertiesHolder.setNormalizeImage(false);
            if(quadsCheckBox.isSelected()) propertiesHolder.setFilterQuads(true);
            else propertiesHolder. setFilterQuads(false);

        } catch(NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Inserted values are invalid");
            alert.showAndWait();
        }
    }

    public void setPropertiesHolder(PropertiesHolder propertiesHolder) {
        super.setPropertiesHolder(propertiesHolder);
        this.propertiesHolder = propertiesHolder;
        pointsHorizontalTextField.setText(String.valueOf(propertiesHolder.getPointsHorizontal()));
        pointsVerticalTextField.setText(String.valueOf(propertiesHolder.getPointsVertical()));
        fastCheckBox.setSelected(propertiesHolder.isFastCheck());
        adaptiveCheckBox.setSelected(propertiesHolder.isAdaptiveThreshold());
        normalizeCheckBox.setSelected(propertiesHolder.isNormalizeImage());
        quadsCheckBox.setSelected(propertiesHolder.isFilterQuads());
    }
}