package pl.golebiewski.controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import pl.golebiewski.dataholders.ResultsHolder;
import pl.golebiewski.enums.StereoCalibrationParameters;
import pl.golebiewski.tools.*;
import pl.golebiewski.dataholders.PropertiesHolder;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class StereoStageController implements Initializable {

    @FXML MenuItem projectPropertiesMenuItem;
    @FXML MenuItem saveReportMenuItem;
    @FXML MenuItem saveLeftUndistortedMenuItem;
    @FXML MenuItem saveRightUndistortedMenuItem;
    @FXML MenuItem saveLeftCalibMenuItem;
    @FXML MenuItem saveRightCalibMenuItem;
    @FXML MenuItem aboutMenuItem;

    @FXML Button loadLeftButton;
    @FXML Button loadRightButton;
    @FXML Button findPatternButton;
    @FXML Button calibrateButton;

    @FXML VBox leftImagesVBox;
    @FXML VBox rightImagesVBox;
    @FXML TextFlow resultsTextFlow;
    @FXML ProgressBar progressBar;

    private List<Image> leftImageList;
    private List<Image> rightImageList;
    private Size leftSize;
    private Size rightSize;
    private List<Mat> leftImagePointsList;
    private List<Mat> rightImagePointsList;
    private List<Mat> objectPointsList;
    private List<File> leftFileList;
    private List<File> rightFileList;
    private List<Boolean> successList;

    private StageCreator stageCreator;
    private PropertiesHolder propertiesHolder;
    private ResultsHolder leftCameraResultsHolder;
    private ResultsHolder rightCameraResultsHolder;

    private enum Side {
        LEFT,
        RIGHT
    }

    public StereoStageController() {
        this.stageCreator = new StageCreator();
        this.propertiesHolder = new PropertiesHolder();
        this.leftCameraResultsHolder = new ResultsHolder();
        this.rightCameraResultsHolder = new ResultsHolder();
        this.leftImageList = new ArrayList<>();
        this.rightImageList = new ArrayList<>();
        this.leftImagePointsList = new ArrayList<>();
        this.rightImagePointsList = new ArrayList<>();
        this.objectPointsList = new ArrayList<>();
        this.leftFileList = new ArrayList<>();
        this.rightFileList = new ArrayList<>();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Menu events
        projectPropertiesMenuItem.setOnAction(EventHandler -> stageCreator.createStereoProperties(this.propertiesHolder));
        saveReportMenuItem.setOnAction(EventHandler -> saveReport());
        saveLeftUndistortedMenuItem.setOnAction(EventHandler -> saveLeftUndistortedImages());
        saveRightUndistortedMenuItem.setOnAction(EventHandler -> saveRightUndistortedImages());
        saveLeftCalibMenuItem.setOnAction(EventHandler -> saveLeftCalibFile());
        saveRightCalibMenuItem.setOnAction(EventHandler -> saveRightCalibFile());
        aboutMenuItem.setOnAction(EventHandler -> stageCreator.createAboutStage());

        //Button events
        loadLeftButton.setOnAction(EventHandler -> initializeView(Side.LEFT));
        loadRightButton.setOnAction(EventHandler -> initializeView(Side.RIGHT));
        findPatternButton.setOnAction(EventHandler -> findImagePoints());
        calibrateButton.setOnAction(EventHandler -> calibrateCamera());
    }

    private void initializeView(Side side) {

        List<File> fileList = DataLoader.loadImages(loadLeftButton.getScene().getWindow());

        if(fileList != null) {
            if(side == Side.LEFT) {
                for (File f : fileList) {
                    Image img = new Image("file:" + f.getAbsolutePath(), 1000D, 1000D, true, false);
                    insertImageInVBox(img, Side.LEFT, f.getName());
                    leftFileList.add(f);
                    }
                Mat m = Imgcodecs.imread(fileList.get(0).getAbsolutePath());
                leftSize = new Size(m.width(), m.height());
            }
            else if(side == Side.RIGHT) {
                for (File f : fileList) {
                    Image img = new Image("file:" + f.getAbsolutePath(), 1000D, 1000D, true, false);
                    insertImageInVBox(img, Side.RIGHT, f.getName());
                    rightFileList.add(f);
                    }
                Mat m = Imgcodecs.imread(fileList.get(0).getAbsolutePath());
                rightSize = new Size(m.width(), m.height());
            }
        }
    }

    private void findImagePoints() {
        if(leftFileList.size() != rightFileList.size()) {
            AlertCreator.errorAlert("Number of images on the left side does not match the right side!", loadLeftButton.getScene().getWindow());
        } else recognizePattern();
    }

    private void recognizePattern() {

        Task<List<Boolean>> task = new Task<List<Boolean>>() {
            @Override
            protected List<Boolean> call() {

                Size boardSize = new Size(propertiesHolder.getPointsHorizontal(), propertiesHolder.getPointsVertical());

                MatOfPoint3f coordinates = new MatOfPoint3f();
                for (int j = 0; j < propertiesHolder.getPointsVertical(); j++) {
                    for (int i = 0; i < propertiesHolder.getPointsHorizontal(); i++) {
                        coordinates.push_back(new MatOfPoint3f(new Point3((float)i, (float)j, 0.00f)));
                    }
                }

                int flags = 0;
                if(propertiesHolder.isFastCheck()) flags += Calib3d.CALIB_CB_FAST_CHECK;
                if(propertiesHolder.isAdaptiveThreshold()) flags += Calib3d.CALIB_CB_ADAPTIVE_THRESH;
                if(propertiesHolder.isNormalizeImage()) flags += Calib3d.CALIB_CB_NORMALIZE_IMAGE;
                if(propertiesHolder.isFilterQuads()) flags += Calib3d.CALIB_CB_FILTER_QUADS;

                List<Boolean> success = new ArrayList<>();
                int iterator = 0;
                for(int i = 0; i < leftFileList.size(); i++) {
                    //find and draw points for left view
                    MatOfPoint2f leftCorners = new MatOfPoint2f();
                    Mat leftMat = Imgcodecs.imread(leftFileList.get(i).getAbsolutePath());
                    boolean leftFound = Calib3d.findChessboardCorners(leftMat, boardSize, leftCorners, flags);
                    Calib3d.drawChessboardCorners(leftMat, boardSize, leftCorners, leftFound);
                    Imgcodecs.imwrite("temp\\left-temp-" + leftFileList.get(i).getName(), leftMat);
                    leftMat.release();
                    Image leftTemp = new Image("file:temp\\left-temp-" + leftFileList.get(i).getName(), 1000D, 1000D, true, false);
                    leftImageList.add(leftTemp);

                    updateProgress(++iterator, leftFileList.size() * 2);

                    //find and draw points for right view
                    MatOfPoint2f rightCorners = new MatOfPoint2f();
                    Mat rightMat = Imgcodecs.imread(rightFileList.get(i).getAbsolutePath());
                    boolean rightFound = Calib3d.findChessboardCorners(rightMat, boardSize, rightCorners, flags);
                    Calib3d.drawChessboardCorners(rightMat, boardSize, rightCorners, rightFound);
                    Imgcodecs.imwrite("temp\\right-temp-" + rightFileList.get(i).getName(), rightMat);
                    rightMat.release();
                    Image rightTemp = new Image("file:temp\\right-temp-" + rightFileList.get(i).getName(), 1000D, 1000D, true, false);
                    rightImageList.add(rightTemp);

                    updateProgress(++iterator, leftFileList.size() * 2);

                    if(leftFound && rightFound) {
                        leftImagePointsList.add(leftCorners);
                        rightImagePointsList.add(rightCorners);
                        objectPointsList.add(coordinates);
                        success.add(true);
                    } else success.add(false);
                }

                return success;
            }
        };

        task.setOnFailed(EventHandler ->
            AlertCreator.errorAlert("Error occurred while recognizing pattern!", loadLeftButton.getScene().getWindow())
        );

        task.setOnSucceeded(EventHandler -> {
            leftImagesVBox.getChildren().clear();
            rightImagesVBox.getChildren().clear();

            successList = task.getValue();
            int pairs = 0;

            for(int i = 0; i < successList.size(); i++) {
                insertImageInVBox(leftImageList.get(i), Side.LEFT, leftFileList.get(i).getName(), successList.get(i));
                insertImageInVBox(rightImageList.get(i), Side.RIGHT, rightFileList.get(i).getName(), successList.get(i));

                if(successList.get(i)) pairs++;
            }

            AlertCreator.informationAlert("Pattern recognized for: " + pairs + " pairs of images", loadLeftButton.getScene().getWindow());
        });

        progressBar.progressProperty().bind(task.progressProperty());
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
    }

    private void calibrateCamera() {
        int flags = 0;
        if(propertiesHolder.isRationalModel()) flags += Calib3d.CALIB_RATIONAL_MODEL;
        if(propertiesHolder.isThinPrismModel()) flags += Calib3d.CALIB_THIN_PRISM_MODEL;
        if(propertiesHolder.isTiltedModel()) flags += Calib3d.CALIB_TILTED_MODEL;
        if(propertiesHolder.getStereoCalibrationParameters()  == StereoCalibrationParameters.SAME_CAMERA)
            flags += Calib3d.CALIB_SAME_FOCAL_LENGTH;

        double calibrationError = 0.0;

        switch(propertiesHolder.getCalibrationParameters()) {
            case SIMPLE:
                calibrationError = Calib3d.stereoCalibrate(
                        objectPointsList,
                        leftImagePointsList,
                        rightImagePointsList,
                        leftCameraResultsHolder.getCameraMatrix(),
                        leftCameraResultsHolder.getDistortionCoefficients(),
                        rightCameraResultsHolder.getCameraMatrix(),
                        rightCameraResultsHolder.getDistortionCoefficients(),
                        leftSize,
                        leftCameraResultsHolder.getRotationMatrix(),
                        leftCameraResultsHolder.getTranslationVector(),
                        leftCameraResultsHolder.getEssentialMatrix(),
                        leftCameraResultsHolder.getFundamentalMatrix(),
                        flags
                );
                break;
            case EXTENDED:
                calibrationError = Calib3d.stereoCalibrateExtended(
                        objectPointsList,
                        leftImagePointsList,
                        rightImagePointsList,
                        leftCameraResultsHolder.getCameraMatrix(),
                        leftCameraResultsHolder.getDistortionCoefficients(),
                        rightCameraResultsHolder.getCameraMatrix(),
                        rightCameraResultsHolder.getDistortionCoefficients(),
                        leftSize,
                        leftCameraResultsHolder.getRotationMatrix(),
                        leftCameraResultsHolder.getTranslationVector(),
                        leftCameraResultsHolder.getEssentialMatrix(),
                        leftCameraResultsHolder.getFundamentalMatrix(),
                        leftCameraResultsHolder.getPerViewErrors(),
                        flags
                );
                break;
        }
        leftCameraResultsHolder.setRmsCalibrationError(calibrationError);

        ResultsPrinter resultsPrinter = new ResultsPrinter(
                leftFileList,
                rightFileList,
                successList,
                propertiesHolder,
                leftCameraResultsHolder,
                rightCameraResultsHolder,
                leftSize,
                rightSize
        );
        resultsTextFlow.getChildren().addAll(resultsPrinter.printStereoResults());
        undistortImages();
    }

    private void undistortImages() {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() {
                leftImageList.clear();
                rightImageList.clear();
                for(int i = 0; i < leftFileList.size(); i++) {
                    Mat leftDistorted = Imgcodecs.imread(leftFileList.get(i).getAbsolutePath());
                    Mat rightDistorted = Imgcodecs.imread(rightFileList.get(i).getAbsolutePath());
                    Mat leftUndistorted = new Mat();
                    Mat rightUndistorted = new Mat();
                    Imgproc.undistort(
                            leftDistorted,
                            leftUndistorted,
                            leftCameraResultsHolder.getCameraMatrix(),
                            leftCameraResultsHolder.getDistortionCoefficients()
                    );
                    Imgproc.undistort(
                            rightDistorted,
                            rightUndistorted,
                            rightCameraResultsHolder.getCameraMatrix(),
                            rightCameraResultsHolder.getDistortionCoefficients()
                    );
                    Imgcodecs.imwrite("temp\\left-temp-" + leftFileList.get(i).getName(), leftUndistorted);
                    Imgcodecs.imwrite("temp\\right-temp-" + rightFileList.get(i).getName(), rightUndistorted);

                    Image leftUndistortedImage = new Image("file:temp\\left-temp-" + leftFileList.get(i).getName(), 1000D, 1000D, true, false);
                    Image rightUndistortedImage = new Image("file:temp\\right-temp-" + rightFileList.get(i).getName(), 1000D, 1000D, true, false);

                    leftImageList.add(leftUndistortedImage);
                    rightImageList.add(rightUndistortedImage);

                    leftDistorted.release();
                    leftUndistorted.release();
                    rightDistorted.release();
                    leftUndistorted.release();

                    File leftFileDelete = new File("temp\\left-temp-" + leftFileList.get(i).getName());
                    File rightFileDelete = new File("temp\\right-temp-" + rightFileList.get(i).getName());
                    //noinspection ResultOfMethodCallIgnored
                    leftFileDelete.delete();
                    //noinspection ResultOfMethodCallIgnored
                    rightFileDelete.delete();

                    double k = i + 1;
                    updateProgress(k, leftFileList.size());
                }
                return null;
            }
        };

        task.setOnFailed(EventHandler ->
            AlertCreator.errorAlert("Error occurred while undistorting images!", calibrateButton.getScene().getWindow())
        );

        task.setOnSucceeded(EventHandler -> {
            rightImagesVBox.getChildren().clear();
            leftImagesVBox.getChildren().clear();

            for(int i = 0; i < leftFileList.size(); i++) {
                insertImageInVBox(leftImageList.get(i), Side.LEFT, "Undistorted-" + leftFileList.get(i).getName());
                insertImageInVBox(rightImageList.get(i), Side.RIGHT, "Undistorted-" + rightFileList.get(i).getName());
            }
        });

        progressBar.progressProperty().bind(task.progressProperty());
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
    }

    private void saveLeftUndistortedImages() {
        ImageSaver.saveUndistortedImages(
                loadLeftButton.getScene().getWindow(),
                leftFileList,
                leftCameraResultsHolder.getCameraMatrix(),
                leftCameraResultsHolder.getDistortionCoefficients()
        );
    }

    private void saveRightUndistortedImages() {
        //list of undistorted images is always empty
        ImageSaver.saveUndistortedImages(
                loadRightButton.getScene().getWindow(),
                rightFileList,
                rightCameraResultsHolder.getCameraMatrix(),
                rightCameraResultsHolder.getDistortionCoefficients());
    }

    private void saveReport() {
        ResultsPrinter resultsPrinter = new ResultsPrinter(
                leftFileList,
                rightFileList,
                successList,
                propertiesHolder,
                leftCameraResultsHolder,
                rightCameraResultsHolder,
                leftSize,
                rightSize
        );
        resultsPrinter.printStereoResultsToFile(loadLeftButton.getScene().getWindow());
    }

    private void saveLeftCalibFile() {
        ResultsPrinter resultsPrinter = new ResultsPrinter(leftFileList, successList, propertiesHolder, leftCameraResultsHolder, leftSize);
        resultsPrinter.saveCalibFile(loadLeftButton.getScene().getWindow());
    }

    private void saveRightCalibFile() {
        ResultsPrinter resultsPrinter = new ResultsPrinter(rightFileList, successList, propertiesHolder, rightCameraResultsHolder, rightSize);
        resultsPrinter.saveCalibFile(loadRightButton.getScene().getWindow());
    }

    private void insertImageInVBox(Image img, Side side, String imageName) {
        Label label = new Label(imageName);
        label.setPrefWidth(leftImagesVBox.getWidth());
        label.setAlignment(Pos.CENTER);

        insertImageInVBoxCommonPart(img, side, label);
    }

    private void insertImageInVBox(Image img, Side side, String imageName, boolean success) {
        Label label = new Label(imageName);
        label.setPrefWidth(leftImagesVBox.getWidth());
        label.setAlignment(Pos.CENTER);
        if(success) label.setTextFill(Color.web("#22cc22"));
        else label.setTextFill(Color.web("#e01818"));

        insertImageInVBoxCommonPart(img, side, label);
    }

    private void insertImageInVBoxCommonPart(Image img, Side side, Label label) {
        ImageView imageView = new ImageView();
        double ratio = img.getWidth() / leftImagesVBox.getWidth();
        imageView.setFitHeight(img.getHeight() / ratio);
        imageView.setFitWidth(leftImagesVBox.getWidth());
        imageView.setImage(img);
        imageView.setOnMouseClicked(MouseEvent -> stageCreator.createImageViewer(img));
        imageView.setCursor(Cursor.HAND);
        if(side == Side.LEFT) {
            leftImagesVBox.getChildren().add(label);
            leftImagesVBox.getChildren().add(imageView);
        }
        else if(side == Side.RIGHT) {
            rightImagesVBox.getChildren().add(label);
            rightImagesVBox.getChildren().add(imageView);
        }
    }

}