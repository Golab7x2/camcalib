package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import java.net.URL;
import java.util.ResourceBundle;

public class AboutStageController implements Initializable {

    @FXML Button closeButton;
    @FXML TextFlow textFlow;

    public AboutStageController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        closeButton.setOnAction(e -> closeButton.getScene().getWindow().hide());

        Text t1 = new Text("CamCalib is based on OpenCV library (version 3.4.1). OpenCV is redistributed and used under " +
                "the terms of 3-clause BSD License (https://opencv.org/license.html). " +
                "\nIcons under the Creative Commons Attribution-NoDerivs 3.0 Unported license by icons8.com\n\n");
        Text t2 = new Text("CamCalib written by Jakub Gołębiewski, Military Academy of Technology, 2018");


        textFlow.getChildren().addAll(t1, t2);
    }

}
