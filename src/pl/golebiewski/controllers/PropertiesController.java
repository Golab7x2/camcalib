package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pl.golebiewski.dataholders.PropertiesHolder;
import pl.golebiewski.enums.CalibrationParameters;
import pl.golebiewski.enums.SensorParameters;

import java.net.URL;
import java.util.ResourceBundle;

public class PropertiesController implements Initializable {

    @FXML RadioButton unknownRadioButton;
    @FXML RadioButton pixelSizeRadioButton;
    @FXML RadioButton sensorDimensionsRadioButton;
    @FXML TextField pixelSizeTextField;
    @FXML TextField sensorHeightTextField;
    @FXML TextField sensorWidthTextField;

    @FXML RadioButton extendedRadioButton;
    @FXML RadioButton simpleRadioButton;

    @FXML CheckBox rationalModelCheckBox;
    @FXML CheckBox thinPrismModelCheckBox;
    @FXML CheckBox tiltedModelCheckBox;

    @FXML Button saveButton;
    @FXML Button cancelButton;

    private PropertiesHolder propertiesHolder;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saveButton.setOnAction(EventHandler -> saveProperties());
        cancelButton.setOnAction(EventHandler -> closeWindow());
    }

    public void saveProperties() {
        try {
            //Sensor parameters
            if(pixelSizeRadioButton.isSelected()) {
                double pixelSize = Double.parseDouble(pixelSizeTextField.getText());
                propertiesHolder.setPixelSize(pixelSize);
                propertiesHolder.setSensorParameters(SensorParameters.PIXEL_SIZE);
            }
            else if(sensorDimensionsRadioButton.isSelected()) {
                double sensorHeight = Double.parseDouble(sensorHeightTextField.getText());
                double sensorWidth = Double.parseDouble(sensorWidthTextField.getText());
                propertiesHolder.setSensorHeight(sensorHeight);
                propertiesHolder.setSensorWidth(sensorWidth);
                propertiesHolder.setSensorParameters(SensorParameters.SENSOR_DIMENSIONS);
            }
            else if(unknownRadioButton.isSelected())
                propertiesHolder.setSensorParameters(SensorParameters.UNKNOWN);

            //Calibration
            if(simpleRadioButton.isSelected())
                propertiesHolder.setCalibrationParameters(CalibrationParameters.SIMPLE);
            else if(extendedRadioButton.isSelected())
                propertiesHolder.setCalibrationParameters(CalibrationParameters.EXTENDED);

            //Distortion
            if(rationalModelCheckBox.isSelected()) propertiesHolder.setRationalModel(true);
            else propertiesHolder.setRationalModel(false);

            if(thinPrismModelCheckBox.isSelected()) propertiesHolder.setThinPrismModel(true);
            else propertiesHolder.setThinPrismModel(false);

            if(tiltedModelCheckBox.isSelected()) propertiesHolder.setTiltedModel(true);
            else propertiesHolder.setTiltedModel(false);


        } catch(NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Inserted values are invalid");
            alert.showAndWait();
        }
    }

    private void closeWindow() {
        cancelButton.getScene().getWindow().hide();
    }

    public void setPropertiesHolder(PropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;

        switch(propertiesHolder.getSensorParameters()) {
            case PIXEL_SIZE:
                pixelSizeRadioButton.setSelected(true);
                pixelSizeTextField.setText(String.valueOf(propertiesHolder.getPixelSize()));
                break;
            case SENSOR_DIMENSIONS:
                sensorDimensionsRadioButton.setSelected(true);
                sensorHeightTextField.setText(String.valueOf(propertiesHolder.getSensorHeight()));
                sensorWidthTextField.setText(String.valueOf(propertiesHolder.getSensorWidth()));
                break;
            case UNKNOWN:
                unknownRadioButton.setSelected(true);
                break;
        }

        switch(propertiesHolder.getCalibrationParameters()) {
            case SIMPLE:
                simpleRadioButton.setSelected(true);
                break;
            case EXTENDED:
                extendedRadioButton.setSelected(true);
                break;
        }

        rationalModelCheckBox.setSelected(propertiesHolder.isRationalModel());
        thinPrismModelCheckBox.setSelected(propertiesHolder.isThinPrismModel());
        tiltedModelCheckBox.setSelected(propertiesHolder.isTiltedModel());
    }
}