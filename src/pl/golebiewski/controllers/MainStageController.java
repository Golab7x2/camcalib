package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import pl.golebiewski.tools.StageCreator;

import java.net.URL;
import java.util.ResourceBundle;

public class MainStageController implements Initializable {

    @FXML Button automaticButton;
    @FXML Button stereoButton;
    @FXML Button undistortButton;

    private StageCreator stageCreator;


    public MainStageController() {
        stageCreator = new StageCreator();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        automaticButton.setOnAction(EventHandler ->
            stageCreator.createMainModuleStage(
                    "CamCalib - Automatic Camera Calibration",
                    "/pl/golebiewski/stages/automatic-stage.fxml",
                    1100,
                    800
            )
        );
        stereoButton.setOnAction(EventHandler ->
            stageCreator.createMainModuleStage(
                    "CamCalib - Stereo Camera Calibration",
                    "/pl/golebiewski/stages/stereo-stage.fxml",
                    1100,
                    800
            )
        );
        undistortButton.setOnAction(EventHandler ->
            stageCreator.createMainModuleStage(
                    "Delete Distortion",
                    "/pl/golebiewski/stages/delete-distortion-stage.fxml",
                    420,
                    180
            ));
    }

}