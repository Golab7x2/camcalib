package pl.golebiewski.controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import pl.golebiewski.dataholders.PropertiesHolder;
import pl.golebiewski.dataholders.ResultsHolder;
import pl.golebiewski.tools.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AutomaticStageController implements Initializable {

    @FXML MenuItem showPropertiesMenuItem;
    @FXML MenuItem saveReportMenuItem;
    @FXML MenuItem saveUndistortedMenuItem;
    @FXML MenuItem saveCalibMenuItem;
    @FXML MenuItem aboutMenuItem;
    @FXML Button loadButton;
    @FXML Button findButton;
    @FXML Button calibrateButton;
    @FXML ProgressBar progressBar;
    @FXML TextFlow resultsTextFlow;
    @FXML VBox imagesVBox;

    private List<Image> imageList;
    private Size imageSize;
    private List<Mat> imagePointsList;
    private List<Mat> objectPointsList;
    private List<File> fileList;
    private List<Boolean> successList;

    private StageCreator stageCreator;
    private ResultsHolder resultsHolder;
    private PropertiesHolder propertiesHolder;


    @SuppressWarnings("WeakerAccess")
    public AutomaticStageController() {
        this.imageList = new ArrayList<>();
        this.imagePointsList = new ArrayList<>();
        this.objectPointsList = new ArrayList<>();
        this.fileList = new ArrayList<>();
        this.stageCreator = new StageCreator();
        this.propertiesHolder = new PropertiesHolder();
        this.resultsHolder = new ResultsHolder();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Menu events
        showPropertiesMenuItem.setOnAction(EventHandler -> stageCreator.createAutomaticProperties(propertiesHolder));
        saveReportMenuItem.setOnAction(EventHandler -> saveReport());
        saveUndistortedMenuItem.setOnAction(EventHandler -> saveUndistortedImages());
        saveCalibMenuItem.setOnAction(EventHandler -> saveCalibFile());
        aboutMenuItem.setOnAction(EventHandler -> stageCreator.createAboutStage());

        //Button events
        loadButton.setOnAction(EventHandler -> initializeView());
        findButton.setOnAction(EventHandler -> findImagePoints());
        calibrateButton.setOnAction(EventHandler -> calibrateCamera());
    }

    private void initializeView() {
        imagesVBox.getChildren().clear();

        fileList = DataLoader.loadImages(loadButton.getScene().getWindow());
        if (fileList != null) {
            for (File f : fileList) {
                Image img = new Image("file:" + f.getAbsolutePath(), 1000D, 1000D, true, false);
                insertImageInVBox(img, f.getName());
            }
            Mat m = Imgcodecs.imread(fileList.get(0).getAbsolutePath());
            imageSize = new Size(m.width(), m.height());
        }
    }

    private void findImagePoints() {

        Task<List<Boolean>> task = new Task<List<Boolean>>() {
            @Override
            protected List<Boolean> call() {

                Size boardSize = new Size(propertiesHolder.getPointsHorizontal(), propertiesHolder.getPointsVertical());

                MatOfPoint3f coordinates = new MatOfPoint3f();
                for (int j = 0; j < propertiesHolder.getPointsVertical(); j++) {
                    for (int i = 0; i < propertiesHolder.getPointsHorizontal(); i++) {
                        coordinates.push_back(new MatOfPoint3f(new Point3((float)i, (float)j, 0.00f)));
                    }
                }

                int flags = 0;
                if(propertiesHolder.isFastCheck()) flags += Calib3d.CALIB_CB_FAST_CHECK;
                if(propertiesHolder.isAdaptiveThreshold()) flags += Calib3d.CALIB_CB_ADAPTIVE_THRESH;
                if(propertiesHolder.isNormalizeImage()) flags += Calib3d.CALIB_CB_NORMALIZE_IMAGE;
                if(propertiesHolder.isFilterQuads()) flags += Calib3d.CALIB_CB_FILTER_QUADS;

                List<Boolean> success = new ArrayList<>();
                Mat matImage;
                for(int i = 0; i < fileList.size(); i++) {
                    MatOfPoint2f corners = new MatOfPoint2f();

                    matImage = Imgcodecs.imread(fileList.get(i).getAbsolutePath());
                    boolean pointsFound = Calib3d.findChessboardCorners(matImage, boardSize, corners, flags);
                    Calib3d.drawChessboardCorners(matImage, boardSize, corners, pointsFound);
                    Imgcodecs.imwrite("temp\\temp-" + fileList.get(i).getName(), matImage);
                    matImage.release();
                    Image img = new Image("file:temp\\temp-" + fileList.get(i).getName(), 1000D, 1000D, true, false);
                    imageList.add(img);

                    if (pointsFound) {
                        imagePointsList.add(corners);
                        objectPointsList.add(coordinates);
                        success.add(true);
                    } else success.add(false);

                    updateProgress(i+1, fileList.size());
                }

                return success;
            }
        };

        task.setOnFailed(EventHandler ->
            AlertCreator.errorAlert("Error occurred while recognizing pattern!", loadButton.getScene().getWindow())
        );

        task.setOnSucceeded(EventHandler -> {
            imagesVBox.getChildren().clear();

            successList = task.getValue();
            int recognized = 0;
            for(int i = 0; i < successList.size(); i++) {
                insertImageInVBox(imageList.get(i), fileList.get(i).getName(), successList.get(i));
                if(successList.get(i)) recognized++;
            }

            AlertCreator.informationAlert("Pattern recognized for: " + recognized + " images", loadButton.getScene().getWindow());
        });

        progressBar.progressProperty().bind(task.progressProperty());
        Thread th = new Thread(task);
        th.start();
    }

    private void calibrateCamera() {
        int flags = 0;
        if(propertiesHolder.isRationalModel()) flags += Calib3d.CALIB_RATIONAL_MODEL;
        if(propertiesHolder.isThinPrismModel()) flags += Calib3d.CALIB_THIN_PRISM_MODEL;
        if(propertiesHolder.isTiltedModel()) flags += Calib3d.CALIB_TILTED_MODEL;
        double calibrationError = 0.0;

        switch(propertiesHolder.getCalibrationParameters()) {
            case SIMPLE:
                calibrationError = Calib3d.calibrateCamera(
                        objectPointsList,
                        imagePointsList,
                        imageSize,
                        resultsHolder.getCameraMatrix(),
                        resultsHolder.getDistortionCoefficients(),
                        resultsHolder.getRvecs(),
                        resultsHolder.getTvecs(),
                        flags
                );
                break;
            case EXTENDED:
                calibrationError = Calib3d.calibrateCameraExtended(
                        objectPointsList,
                        imagePointsList,
                        imageSize,
                        resultsHolder.getCameraMatrix(),
                        resultsHolder.getDistortionCoefficients(),
                        resultsHolder.getRvecs(),
                        resultsHolder.getTvecs(),
                        resultsHolder.getStdDevIntrinsics(),
                        resultsHolder.getStdDevExtrinsics(),
                        resultsHolder.getPerViewErrors(),
                        flags
                );
                break;
        }
        resultsHolder.setRmsCalibrationError(calibrationError);

        ResultsPrinter resultsPrinter = new ResultsPrinter(fileList, successList, propertiesHolder, resultsHolder, imageSize);
        resultsTextFlow.getChildren().addAll(resultsPrinter.printResults());
        undistortImages();
    }

    private void undistortImages() {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() {
                imageList.clear();
                double i = 0D;
                for (File f : fileList) {
                    Mat distorted = Imgcodecs.imread(f.getAbsolutePath());
                    Mat undistorted = new Mat();
                    Imgproc.undistort(
                            distorted,
                            undistorted,
                            resultsHolder.getCameraMatrix(),
                            resultsHolder.getDistortionCoefficients()
                    );
                    Imgcodecs.imwrite("temp\\temp-" + f.getName(), undistorted);
                    Image undistortedImage = new Image("file:temp\\temp-" + f.getName(), 1000D, 1000D, true, false);
                    imageList.add(undistortedImage);

                    distorted.release();
                    undistorted.release();
                    File fileDelete = new File("temp\\temp-" + f.getName());
                    //noinspection ResultOfMethodCallIgnored
                    fileDelete.delete();
                    updateProgress(++i, fileList.size());
                }
                return null;
            }
        };

        task.setOnFailed(EventHandler ->
            AlertCreator.errorAlert("Error occurred while undistorting images!", calibrateButton.getScene().getWindow())
        );

        task.setOnSucceeded(EventHandler -> {
            imagesVBox.getChildren().clear();
            for(int i = 0; i < imageList.size(); i++) {
                insertImageInVBox(imageList.get(i), "Undistorted-" + fileList.get(i).getName());
            }
        });

        progressBar.progressProperty().bind(task.progressProperty());
        Thread th = new Thread(task);
        th.start();
    }

    private void saveUndistortedImages() {
        ImageSaver.saveUndistortedImages(
                loadButton.getScene().getWindow(),
                fileList,
                resultsHolder.getCameraMatrix(),
                resultsHolder.getDistortionCoefficients()
        );
    }

    private void saveReport() {
        ResultsPrinter resultsPrinter = new ResultsPrinter(fileList, successList, propertiesHolder, resultsHolder, imageSize);
        resultsPrinter.printResultsToFile(loadButton.getScene().getWindow());
    }

    private void saveCalibFile() {
        ResultsPrinter resultsPrinter = new ResultsPrinter(fileList, successList, propertiesHolder, resultsHolder, imageSize);
        resultsPrinter.saveCalibFile(loadButton.getScene().getWindow());
    }

    private void insertImageInVBox(Image img, String imageName) {
        Label label = new Label(imageName);
        label.setPrefWidth(imagesVBox.getWidth());
        label.setAlignment(Pos.CENTER);
        insertImageInVBoxCommonPart(img, label);
    }

    private void insertImageInVBox(Image img, String imageName, boolean success) {
        Label label = new Label(imageName);
        label.setPrefWidth(imagesVBox.getWidth());
        label.setAlignment(Pos.CENTER);
        if(success) label.setTextFill(Color.web("#22cc22"));
        else label.setTextFill(Color.web("#e01818"));
        insertImageInVBoxCommonPart(img, label);
    }

    private void insertImageInVBoxCommonPart(Image img, Label label) {
        ImageView imageView = new ImageView();
        double ratio = img.getWidth() / imagesVBox.getWidth();
        imageView.setFitWidth(imagesVBox.getWidth());
        imageView.setFitHeight(img.getHeight() / ratio);
        imageView.setImage(img);
        imageView.setOnMouseClicked(MouseEvent -> stageCreator.createImageViewer(img));
        imageView.setCursor(Cursor.HAND);
        imagesVBox.getChildren().add(label);
        imagesVBox.getChildren().add(imageView);
    }

}
