package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import pl.golebiewski.enums.StereoCalibrationParameters;
import pl.golebiewski.dataholders.PropertiesHolder;

import java.net.URL;
import java.util.ResourceBundle;

public class StereoPropertiesController extends AutomaticPropertiesController implements Initializable {

    @FXML RadioButton unknownRadioButton;
    @FXML RadioButton pixelSizeRadioButton;
    @FXML RadioButton sensorDimensionsRadioButton;

    @FXML TextField rightPixelSizeTextField;
    @FXML TextField rightSensorHeightTextField;
    @FXML TextField rightSensorWidthTextField;

    @FXML RadioButton sameSensorRadioButton;
    @FXML RadioButton differentCamerasRadioButton;

    private PropertiesHolder propertiesHolder;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
    }

    @Override
    public void saveProperties() {
        super.saveProperties();
        if(sameSensorRadioButton.isSelected())
            propertiesHolder.setStereoCalibrationParameters(StereoCalibrationParameters.SAME_CAMERA);
        else if(differentCamerasRadioButton.isSelected())
            propertiesHolder.setStereoCalibrationParameters(StereoCalibrationParameters.DIFFERENT_CAMERAS);

        try {
            if(pixelSizeRadioButton.isSelected()) {
                propertiesHolder.setRightPixelSize(Double.parseDouble(rightPixelSizeTextField.getText()));
            }
            else if(sensorDimensionsRadioButton.isSelected()) {
                propertiesHolder.setRightSensorHeight(Double.parseDouble(rightSensorHeightTextField.getText()));
                propertiesHolder.setRightSensorWidth(Double.parseDouble(rightSensorWidthTextField.getText()));
            }

        } catch(NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Inserted values are invalid");
            alert.showAndWait();
        }
    }

    @Override
    public void setPropertiesHolder(PropertiesHolder propertiesHolder) {
        super.setPropertiesHolder(propertiesHolder);
        this.propertiesHolder = propertiesHolder;
        switch(propertiesHolder.getStereoCalibrationParameters()) {
            case SAME_CAMERA:
                sameSensorRadioButton.setSelected(true);
                break;
            case DIFFERENT_CAMERAS:
                differentCamerasRadioButton.setSelected(true);
                break;
        }

        switch(propertiesHolder.getSensorParameters()) {
            case PIXEL_SIZE:
                rightPixelSizeTextField.setText(String.valueOf(propertiesHolder.getRightPixelSize()));
                break;
            case SENSOR_DIMENSIONS:
                rightSensorHeightTextField.setText(String.valueOf(propertiesHolder.getRightSensorHeight()));
                rightSensorWidthTextField.setText(String.valueOf(propertiesHolder.getRightSensorWidth()));
                break;
        }
    }
}