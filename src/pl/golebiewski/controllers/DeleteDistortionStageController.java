package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import pl.golebiewski.tools.AlertCreator;
import pl.golebiewski.tools.DataLoader;
import pl.golebiewski.tools.ImageSaver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DeleteDistortionStageController implements Initializable {

    @FXML Button loadCalibButton;
    @FXML Button loadImagesButton;
    @FXML Button undistortButton;
    @FXML Label calibLabel;
    @FXML Label imagesLabel;

    private Mat cameraMatrix;
    private Mat distortionCoefficients;
    private List<File> fileList;

    private boolean calibOK;
    private boolean imagesOK;

    public DeleteDistortionStageController() {
        this.calibOK = false;
        this.imagesOK = false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       loadCalibButton.setOnAction(EventHandler -> loadCalibFile());
       loadImagesButton.setOnAction(EventHandler -> loadImages());
       undistortButton.setOnAction(EventHandler -> deleteDistortion());
       undistortButton.setDisable(true);
    }

    private void loadCalibFile() {
        File calibFile = DataLoader.loadCalibFile(loadCalibButton.getScene().getWindow());
        if(calibFile != null) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(calibFile.getAbsolutePath()));
                String[] data = reader.readLine().split(",");

                //initialize camera matrix
                cameraMatrix = new Mat(3, 3, CvType.CV_32FC1);
                cameraMatrix.put(0, 0, Double.parseDouble(data[0]));
                cameraMatrix.put(1, 1, Double.parseDouble(data[1]));
                cameraMatrix.put(0, 2, Double.parseDouble(data[2]));
                cameraMatrix.put(1, 2, Double.parseDouble(data[3]));
                cameraMatrix.put(2, 2, 1);

                //initialize distortion vector
                distortionCoefficients = new Mat(1, data.length - 4, CvType.CV_32FC1);
                for(int i = 4; i < data.length; i++) {
                    distortionCoefficients.put(0, i - 4, Double.parseDouble(data[i]));
                }

                calibOK = true;
                calibLabel.setText(calibFile.getName());
                reader.close();
            }
            catch (Exception e) {
                AlertCreator.errorAlert("Error occurred while reading file!", loadCalibButton.getScene().getWindow());
            }
        }

        if(calibOK && imagesOK) undistortButton.setDisable(false);
    }

    private void loadImages() {
        fileList = DataLoader.loadImages(loadImagesButton.getScene().getWindow());
        if(fileList != null) {
            imagesOK = true;
            imagesLabel.setText(fileList.size() + " images loaded");
        }
        if(calibOK && imagesOK) undistortButton.setDisable(false);
    }

    private void deleteDistortion() {
        ImageSaver.saveUndistortedImages(loadImagesButton.getScene().getWindow(), fileList, cameraMatrix, distortionCoefficients);
    }
}