package pl.golebiewski.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ImageViewerController implements Initializable {

    @FXML ScrollPane scrollPane;
    private ImageView imageView;

    private double width;
    private double height;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       imageView = new ImageView();
       imageView.setCursor(Cursor.HAND);
       imageView.setOnMouseClicked(this::zoomImage);
    }

    private void zoomImage(MouseEvent m) {

        if (m.getButton() == MouseButton.PRIMARY) {
            width *= 1.5;
            height *= 1.5;
        }
        if (m.getButton() == MouseButton.SECONDARY) {
            width /= 1.5;
            height /= 1.5;
        }
        if(m.getButton() == MouseButton.MIDDLE) {
            double positionX = scrollPane.getHvalue() * width - scrollPane.getWidth() * scrollPane.getHvalue();
            double centerX = positionX + scrollPane.getWidth()*0.5;
            double dx = m.getX() - centerX;
            double posXok = positionX + dx;
            scrollPane.setHvalue(posXok / (width - scrollPane.getWidth()));
            double positionY = scrollPane.getVvalue() * height - scrollPane.getHeight() * scrollPane.getVvalue();
            double centerY = positionY + scrollPane.getHeight()*0.5;
            double dy = m.getY() - centerY;
            double posYok = positionY + dy;
            scrollPane.setVvalue(posYok / (height - scrollPane.getHeight()));
        }

        imageView.setFitHeight(height);
        imageView.setFitWidth(width);
    }

    public void setImage(Image image) {
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.imageView.setImage(image);
        this.scrollPane.setContent(imageView);
    }

}
