package pl.golebiewski;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.opencv.core.Core;
import java.io.File;


public class CamCalibApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/pl/golebiewski/stages/main-stage.fxml"));
        primaryStage.setTitle("CamCalib");
        primaryStage.getIcons().add(new Image("/resources/icon-camera.png"));
        primaryStage.setScene(new Scene(root, 280, 170));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        File dir = new File("temp");
        //noinspection ResultOfMethodCallIgnored
        dir.mkdir();
        launch(args);
    }

}
